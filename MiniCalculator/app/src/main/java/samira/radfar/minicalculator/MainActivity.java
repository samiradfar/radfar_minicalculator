package samira.radfar.minicalculator;

import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tv;

    Button btn0;
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;
    Button btn9;

    Button dot;
    Button equal;
    Button mines;
    Button multiple;
    Button plus;
    Button devide;
    Button clean;
    Button ac;

    float num1 = 0 ;
    String oprator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tv = (TextView) findViewById(R.id.tv);

        btn0 = (Button) findViewById(R.id.btn0);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);

        dot = (Button) findViewById(R.id.dot);
        equal = (Button) findViewById(R.id.equal);
        mines = (Button) findViewById(R.id.mines);
        multiple = (Button) findViewById(R.id.multiple);
        plus = (Button) findViewById(R.id.plus);
        devide = (Button) findViewById(R.id.devide) ;
        clean = (Button) findViewById(R.id.clean);
        ac = (Button) findViewById(R.id.ac);

        btn0.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);

        dot.setOnClickListener(this);
        equal.setOnClickListener(this);
        mines.setOnClickListener(this);
        plus.setOnClickListener(this);
        multiple.setOnClickListener(this);
        devide.setOnClickListener(this);
        clean.setOnClickListener(this);
        ac.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.btn0:
                show("0");
                break;

            case R.id.btn1:
                show("1");
                break;

            case R.id.btn2:
                show("2");
                break;

            case R.id.btn3:
                show("3");
                break;

            case R.id.btn4:
                show("4");
                break;

            case R.id.btn5:
                show("5");
                break;

            case R.id.btn6:
                show("6");
                break;

            case R.id.btn7:
                show("7");
                break;

            case R.id.btn8:
                show("8");
                break;

            case R.id.btn9:
                show("9");
                break;

            case R.id.dot:
                show(".");
                break;

            case R.id.plus:
                operateReady();
                oprator="+";
                break;

            case R.id.mines:
                operateReady();
                oprator="-";
                break;

            case R.id.multiple:
                operateReady();
                oprator="*";
                break;

            case R.id.devide:
                operateReady();
                oprator="/";
                break;

            case R.id.equal:
                calculate();
                break;


        }
            }

    private void calculate(){

        float num2=Float.parseFloat(tv.getText().toString());
        float resault = 0;
        tv.setText("");

        switch (oprator) {
            case "+":
                resault=plus(num1,num2);
                break;

            case "-":
                resault=mines(num1,num2);
                break;

            case "*":
                resault=multiply(num1,num2);
                break;

            case "/":
                resault=division(num1,num2);
                break;

        }

        show(String.valueOf(resault));
    }

    private float plus(float no1, float no2){

        return no1+no2;
    }

    private float mines(float no1, float no2){

        return no1-no2;
    }

    private float multiply(float no1, float no2){

        return no1*no2;
    }

    private float division(float no1, float no2){

        return no1/no2;
    }


    private void operateReady (){

        num1=Float.parseFloat(tv.getText().toString());
        tv.setText("");
    }

    private void show(String n){

        String current = tv.getText().toString();
        if (current.equals("0")){
            if (n.equals("."))
                tv.append(n);
            else
                tv.setText(n);
        }
        else {
/*            if (n.equals("."))
                if ()*/
            tv.append(n);
        }


/*        if (n.equals(".")){
            current = tv.getText().toString();
            if (current.equals("0")){
                tv.setText(n);
            }
        }*/
    }

}
